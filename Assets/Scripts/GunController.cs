﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public GameObject Bullet;
    public GameObject Player;
    public int Damage;
    public float RecoilForce;
    public SpriteRenderer SpriteRenderer;
    public Transform PlayerTrasform;
    public GameObject ShotPoint;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        changeDirection();
        if (Input.GetButtonDown("Fire1"))
        {
            if (LevelManager.Instance.WeaponsController!=null&& LevelManager.Instance.WeaponsController.canShot())
            {
                Shot();
            }
        }
        rotate();
    }

    private void Shot()
    {
        GameObject shot = Instantiate(Bullet, ShotPoint.transform.position, Quaternion.identity);
        Vector2 direccio = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
        direccio = direccio.normalized;
        Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x - direccio.x * RecoilForce/10, Camera.main.transform.localPosition.y - direccio.y * RecoilForce/10,-10);
        shot.GetComponent<BulletController>().Shot(direccio, Damage);
        float angle = Mathf.Atan2(direccio.y, direccio.x) * Mathf.Rad2Deg;
        shot.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        Invoke("returnRecoil", 0.1f);
        LevelManager.Instance.WeaponsController.useBullet();
    }
    private void returnRecoil()
    {
        Camera.main.transform.localPosition = new Vector3(0,0,-10);
    }
    private void rotate()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 aim = mousePos - transform.position;
        float angle = Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }
    private void changeDirection()
    {
        if ((Camera.main.ScreenToWorldPoint(Input.mousePosition) - PlayerTrasform.position).x>0)
        {
            SpriteRenderer.flipY = false;
            transform.localPosition = new Vector3(0.3f, transform.localPosition.y, transform.localPosition.z);
            ShotPoint.transform.localPosition = new Vector2(ShotPoint.transform.localPosition.x, Mathf.Abs(ShotPoint.transform.localPosition.y));
        }
        else
        {
            SpriteRenderer.flipY = true;
            transform.localPosition = new Vector3(-0.3f, transform.localPosition.y, transform.localPosition.z);
            ShotPoint.transform.localPosition = new Vector2(ShotPoint.transform.localPosition.x, Mathf.Abs(ShotPoint.transform.localPosition.y) * -1);
        }
        //ShotPoint.transform.localPosition = new Vector2(ShotPoint.transform.localPosition.x, ShotPoint.transform.localPosition.y * -1);
    }
    public void changeWeapon(WeaponSC weapon)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = weapon.WeaponSprite;
        Damage = weapon.Damage;
        RecoilForce = weapon.RecoilForce;
        if (weapon.GetType().Name == "DistanceWeaponSC")
        {
            DistanceWeaponSC weaponDW = (DistanceWeaponSC)weapon;
            Bullet = weaponDW.Bullet;
            ShotPoint.transform.localPosition = weaponDW.OffsetFirePoint;
        }
    }
}
