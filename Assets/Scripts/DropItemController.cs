﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DropItemController : MonoBehaviour
{
    [SerializeField]
    private ItemListDrop itemsList;
    private List<ItemDataSC> itemList;
    public Tilemap TileMap;


    // Start is called before the first frame update
    void Start()
    {
        //Invoke("getRandomItem4Invoke", 1);
        TileMap = gameObject.GetComponent<Tilemap>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            var tilePos = TileMap.WorldToCell(collision.gameObject.transform.position);
            if (TileMap.GetTile(tilePos) != null)
            {
                collision.gameObject.GetComponent<BulletController>().destroyMe();
                //Debug.Log(TileMap.GetTile(tilePos).name);
                TileMap.SetTile(tilePos, null);
                getRandomItem4Invoke(tilePos);
            }
        }else if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            collision.gameObject.GetComponent<BulletController>().destroyMe();
        }
    }
    public void getRandomItem4Invoke(Vector3Int tilePos)
    {
        GameObject item = itemsList.getRandomItem();
        item.transform.position = tilePos + LevelManager.Instance.SalaActual.transform.position;
    }

}
