﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemShopController : ItemController
{
    internal override void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ShopManager.Instance.UIController.setItemData(ItemData.Name, ItemData.Price);
            if (ShopManager.Instance.Player.GetComponent<PlayerMoveController>().isCatching())
            {
                if (ItemData.Price <= GameManager.Instance.Points)
                {
                    GameManager.Instance.Points -= ItemData.Price;
                    ShopManager.Instance.UIController.UpdatePoints();
                    switch (ItemData.GetType().Name)
                    {
                        /*case "HealKitSC":
                            HealKitSC currentItemHK = (HealKitSC)ItemData;
                            LevelManager.Instance.playerGainLive(currentItemHK.Heal);
                            break;*/
                        case "DistanceWeaponSC":
                        case "MeleeWeaponSC":
                            WeaponSC currentItemW = (WeaponSC)ItemData;
                            ShopManager.Instance.Weapons.AvariableWeapons.Remove(currentItemW);
                            ShopManager.Instance.Weapons.InitialWeapons.Add(currentItemW);
                            break;
                    }
                    Destroy(gameObject);
                }
            }
        }
    }
}
