﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    internal int Enemies = 1;
    public GameUIController UIController;
    public WeaponsController WeaponsController;
    public GameObject Player;
    public GameObject Gun;
    public GameObject SalaActual;
    public GameObject[] Sales;
    public int Points = 0;
    public int RoomsPass = 0;
    public int EnemiesKilled = 0;
    public int PointsForComplete;
    private bool ended = false;

    public delegate void OpenDoor();
    public event OpenDoor OpenDoorEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Enemies <= 0)
        {
            if (!ended)
            {
                ended = true;
                addPoints(PointsForComplete);
                RoomsPass++;
                if (OpenDoorEvent != null)
                {
                    OpenDoorEvent();
                }
            }
        }
    }
    public void NextSala()
    {
        Destroy(SalaActual);
        SalaActual = Instantiate(Sales[Random.Range(0, Sales.Length)], Player.transform.position, Quaternion.identity);
        ended = false;
    }
    public void addPoints(int points)
    {
        Points += points;
        UIController.reloadPoints();
    }
    public void playerLostLive(int liveLost)
    {
        Player.GetComponent<PlayerMoveController>().lostLive(liveLost);
        UIController.reloadLive();
    }
    public void playerGainLive(int liveGain)
    {
        Player.GetComponent<PlayerMoveController>().gainLive(liveGain);
        UIController.reloadLive();
    }
    public void playerDead()
    {
        GameManager.Instance.addPoints(Points);
        GameManager.Instance.LastPoints = Points;
        if (GameManager.Instance.MaxPoints < Points)
        {
            GameManager.Instance.MaxPoints= Points;
            UIController.showResult(true);
        }
        else
        {
            UIController.showResult(false);
        }
       
    }
}
