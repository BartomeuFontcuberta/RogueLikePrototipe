﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUIController : MonoBehaviour
{
    public Text Points;
    public Text Bullets;
    public Slider Live;
    public GameObject Result;
    public GameObject BestText;
    public Text ResultPoints;
    public Text ResultEnemies;
    public Text ResultRooms;
    private PlayerMoveController PlController;
    public Image[] Weapons;
    // Start is called before the first frame update
    void Start()
    {
        PlController = LevelManager.Instance.Player.GetComponent<PlayerMoveController>();
        reloadLive();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void reloadPoints()
    {
        Points.text = ("Points: "+ LevelManager.Instance.Points);
    }
    public void reloadBullets(int bullets)
    {
        if (bullets >=0)
        {
            Bullets.text = bullets.ToString();
        }
        else
        {
            Bullets.text = "∞";
        }
        
    }
    public void reloadLive()
    {
        Live.value = (float)PlController.Live/(float)PlController.MaxLive;
    }
    public void updateWeapons(List<WeaponSC> weapons,int activeWeaponIndex)
    {
       
        Weapons[0].sprite = weapons[getWeaponPosition(-2,weapons.Count,activeWeaponIndex)].IconSprite;
        Weapons[1].sprite = weapons[getWeaponPosition(-1, weapons.Count, activeWeaponIndex)].IconSprite;
        Weapons[2].sprite = weapons[activeWeaponIndex].IconSprite;
        Weapons[3].sprite = weapons[getWeaponPosition(1, weapons.Count, activeWeaponIndex)].IconSprite;
        Weapons[4].sprite = weapons[getWeaponPosition(2, weapons.Count, activeWeaponIndex)].IconSprite;
        if (weapons[activeWeaponIndex].GetType().Name == "DistanceWeaponSC")
        {
            DistanceWeaponSC currentItemDW = (DistanceWeaponSC)weapons[activeWeaponIndex];
            reloadBullets(currentItemDW.BulletAmount);
        }
        else
        {
            reloadBullets(-1);
        }

        /*Debug.Log(Weapons.Length+" "+direction);
        for (int i = 0; i < Weapons.Length; i++)
        {
            Debug.Log((i + direction+ Weapons.Length) % Weapons.Length);
            Weapons[i].sprite = Weapons[(i+direction+Weapons.Length)%Weapons.Length].sprite;
        }
        Weapons[(direction+ Weapons.Length) % Weapons.Length].sprite = tempImage;*/
    }
    private int getWeaponPosition(int dif, int size, int originalPosition)
    {
        originalPosition+=dif;
        if (originalPosition > size -1)
        {
            originalPosition = originalPosition%size;
        }
        else if (originalPosition < 0){
            originalPosition = originalPosition + size;
        }
        return originalPosition;
    }
    public void showResult(bool Best)
    {
        Result.SetActive(true);
        ResultPoints.text = ("Points: " + LevelManager.Instance.Points);
        if (Best)
        {
            BestText.SetActive(true);
        }
        ResultEnemies.text = ("Enemies: " + LevelManager.Instance.EnemiesKilled);
        ResultRooms.text = ("Rooms: " + LevelManager.Instance.RoomsPass);
    }
    public void restart()
    {
        SceneManager.LoadScene(1,LoadSceneMode.Single);
    }
    public void goMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
