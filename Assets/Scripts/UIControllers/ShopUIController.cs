﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopUIController : MonoBehaviour
{
    public Text PointsText;
    public Text ItemNameText;
    public Text ItemPriceText;
    // Start is called before the first frame update
    void Start()
    {
        UpdatePoints();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setItemData(string name, int price)
    {
        ItemNameText.text = name;
        ItemPriceText.text = price.ToString();
        if (price <= GameManager.Instance.Points)
        {
            ItemPriceText.color = Color.green;
        }
        else
        {
            ItemPriceText.color = Color.red;
        }
    }
    public void UpdatePoints()
    {
        PointsText.text = GameManager.Instance.Points.ToString();
    }
    public void goMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
