﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUIController : MonoBehaviour
{
    public Text BestPoints;
    public Text LastPoints;
    public Text MuteButtonText;

    // Start is called before the first frame update
    void Start()
    {
        mute();
        mute();
    }

    // Update is called once per frame
    void Update()
    {
        BestPoints.text = GameManager.Instance.MaxPoints.ToString();
        LastPoints.text = GameManager.Instance.LastPoints.ToString();
    }

    public void goGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void goShop()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
    public void mute()
    {
        if(AudioListener.volume!=0){
            AudioListener.volume = 0;
            MuteButtonText.text = "Unmute";
        }
        else
        {
            AudioListener.volume = 1;
            MuteButtonText.text = "Mute";
        }
    }
    public void save()
    {
        DataToSave data = new DataToSave();
        data.Points = GameManager.Instance.Points;
        data.MaxPoints = GameManager.Instance.MaxPoints;
        data.LastPoints = GameManager.Instance.LastPoints;
        PersistentData.SaveData<DataToSave>(data, "PrincipalData");
        Debug.Log("ToSave: "+GameManager.Instance.MaxPoints);
        Debug.Log("ToSave: "+GameManager.Instance.LastPoints);
        Debug.Log("ToSave: "+GameManager.Instance.Points);
    }
}
