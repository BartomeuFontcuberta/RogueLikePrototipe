﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    /*[SerializeField]
    private List<AudioClip> audios;*/
    private AudioSource audioSource;
    public int MaxLive;
    public int Live;
    public float Speed;
    public SpriteRenderer SpriteRenderer;
    public Animator Animator;
    private Color NormalColor;
    public Color DashColor;
    public AudioClip DeadSound;
    Vector2 direccio;
    public int DashSpeed;
    public float DashCoolDown;
    public float DashTime;
    public float InputActiveTime;
    private float waitTime=0;
    private bool inDash = false;
    private bool imCatching = false;
    // Start is called before the first frame update
    void Start()
    {
        NormalColor = SpriteRenderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        direccio = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))*Speed;
        if (Input.GetAxis("Horizontal")==0&& Input.GetAxis("Vertical")==0)
        {
            Animator.SetBool("Walk", false);
        }
        else
        {
            Animator.SetBool("Walk", true);
        }
        if (Input.GetAxis("Dash")>0 && waitTime<=0)
        {
            dash();
        }
        if (Input.GetButtonDown("Catch"))
        {
            imCatching = true;
            Invoke("stopCatch", InputActiveTime);
        }
        waitTime -= Time.deltaTime;
        changeDirection();
    }

    private void FixedUpdate()
    {
        transform.Translate( direccio.x * Time.fixedDeltaTime, direccio.y * Time.fixedDeltaTime, 0);
    }
    private void changeDirection()
    {
        if ((Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).x > 0)
        {
            SpriteRenderer.flipX = true;
        }
        else
        {
            SpriteRenderer.flipX = false;
        }
    }
    public bool isCatching()
    {
        return imCatching;
    }
    public void lostLive(int liveLost)
    {
        if (!inDash)
        {
            //Carregar audioclip en el audioSource
            Live -= liveLost;
            if (Live <= 0)
            {
                LevelManager.Instance.gameObject.GetComponent<AudioSource>().clip = DeadSound;
                LevelManager.Instance.gameObject.GetComponent<AudioSource>().Play();
                LevelManager.Instance.playerDead();
                Instantiate(GameObject.FindGameObjectWithTag("MainCamera"), transform.position + new Vector3(0, 0, -10), Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
    private void dash()
    {
        inDash = true;
        SpriteRenderer.color=DashColor;
        Vector2 direccio = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
        gameObject.GetComponent<Rigidbody2D>().AddForce((direccio.normalized * DashSpeed), ForceMode2D.Impulse);
        waitTime = DashCoolDown;
        Invoke("stopDash",DashTime);
    }
    private void stopDash()
    {
        SpriteRenderer.color = NormalColor;
        inDash = false;
        gameObject.GetComponent<Rigidbody2D>().velocity=Vector2.zero;
    }
    private void stopCatch()
    {
        imCatching = false;
    }
    public void gainLive(int liveGain)
    {
        Live += liveGain;
        if (Live > MaxLive)
        {
            Live = MaxLive;
        }

    }
}
