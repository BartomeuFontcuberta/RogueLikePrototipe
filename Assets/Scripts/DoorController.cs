﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DoorController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LevelManager.Instance.OpenDoorEvent += OpeningDoors;
    }

    private void OpeningDoors() {
        gameObject.GetComponent<TilemapCollider2D>().isTrigger = true;
        LevelManager.Instance.OpenDoorEvent -= OpeningDoors;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            LevelManager.Instance.NextSala();
        }
    }
}
