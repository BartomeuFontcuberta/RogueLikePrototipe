﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject[] Enemies;
    public float MinX, MaxX, MinY, MaxY;
    public int TotalEnemies;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < TotalEnemies; i++)
        {
            Instantiate(Enemies[Random.Range(0, Enemies.Length)], new Vector2(transform.position.x+Random.Range(MinX,MaxX), transform.position.y + Random.Range(MinY,MaxY)), Quaternion.identity);
        }
        LevelManager.Instance.Enemies = TotalEnemies;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
