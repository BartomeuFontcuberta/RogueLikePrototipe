﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : EnemyDeadController
{
    public GameObject Bullet;
    public float WaitTime;
    public int Damage;
    private float wait;
    public GameObject Gun;
    public GameObject ShotPoint;
    // Start is called before the first frame update
    void Start()
    {
        wait = WaitTime;
    }

    // Update is called once per frame
    void Update()
    {
        iAmStunt();
        if (LevelManager.Instance.Player != null)
        {
            if (wait <= 0)
            {
                shot();
                wait = WaitTime;
            }
            wait -= Time.deltaTime;
            rotate();
        }
    }
    private void iAmStunt()
    {
        if (stuntTime > 0)
        {
            wait+=stuntTime;
            stuntTime = 0;
        }
    }
    private void shot()
    {
        GameObject shot = Instantiate(Bullet, ShotPoint.transform.position, Quaternion.identity);
        Vector2 direccio = (LevelManager.Instance.Player.transform.position + new Vector3(0, 0.6f, 0) - Gun.transform.position);
        direccio = direccio.normalized;
        shot.GetComponent<BulletController>().Shot(direccio, Damage);
    }
    private void rotate()
    {
        Vector3 playerPos = LevelManager.Instance.Player.transform.position+new Vector3(0,0.6f,0);
        Vector3 aim = playerPos - Gun.transform.position;
        float angle = Mathf.Atan2(aim.y, aim.x) * Mathf.Rad2Deg;
        Gun.transform.localRotation = Quaternion.Euler(0f, 0f, angle-90);
    }
}
