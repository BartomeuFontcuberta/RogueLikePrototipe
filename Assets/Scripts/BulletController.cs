﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float Force;
    private int damage;
    public bool FromPlayer;
    public float StuntTime=0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //BulletTransform.position.;
    }
    public void Shot(Vector2 direccio, int Damage)
    {
        damage = Damage;
        if (false)
        {
            //Melee
            if (direccio.x > direccio.y)
            {
                if (direccio.x * -1 > direccio.y)
                {

                    gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, -Force, 0), ForceMode2D.Impulse);
                }
                else
                {
                    gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(Force, 0, 0), ForceMode2D.Impulse);
                }
            }
            else
            {
                if (direccio.x * -1 > direccio.y)
                {

                    gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(-Force, 0, 0), ForceMode2D.Impulse);
                }
                else
                {
                    gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, Force, 0), ForceMode2D.Impulse);
                }
            }
        }
        else
        {
            //Distància
            gameObject.GetComponent<Rigidbody2D>().AddForce(direccio * Force, ForceMode2D.Impulse);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Walls"))
        {
            destroyMe();
        }
        else{
            if (FromPlayer && collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponentInChildren<EnemyDeadController>().makeDamage(damage);
                collision.gameObject.GetComponentInChildren<EnemyDeadController>().stuntTime += StuntTime;
                destroyMe();
            } else if (!FromPlayer && collision.gameObject.CompareTag("Player"))
            {
                LevelManager.Instance.playerLostLive(damage);
                destroyMe();
            }
        }
    }

    public void destroyMe()
    {
        Destroy(this.gameObject);
    }

}
