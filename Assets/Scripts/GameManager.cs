﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    //public DataToSave Data;
    public int Points=0;
    public int MaxPoints = 0;
    public int LastPoints = 0;

    // Start is called before the first frame update
    void Start()
    {
        DataToSave toLoadData = PersistentData.LoadData<DataToSave>(new DataToSave(),"PrincipalData");
        Debug.Log(toLoadData);
        if (toLoadData != null)
        {
            Debug.Log("Load: " + toLoadData.MaxPoints);
            Debug.Log("Load: " + toLoadData.LastPoints);
            Debug.Log("Load: " + toLoadData.Points);
            Points = toLoadData.Points;
            MaxPoints = toLoadData.MaxPoints;
            LastPoints = toLoadData.LastPoints;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void addPoints(int points)
    {
        Points += points;
    }
}
