﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public ItemDataSC ItemData;
    public int PointsForEchItemCatch;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = ItemData.IconSprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    internal virtual void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (LevelManager.Instance.Player.GetComponent<PlayerMoveController>().isCatching())
            {
                LevelManager.Instance.addPoints(PointsForEchItemCatch);
                switch (ItemData.GetType().Name)
                {
                    case "HealKitSC":
                        HealKitSC currentItemHK = (HealKitSC)ItemData;
                        LevelManager.Instance.playerGainLive(currentItemHK.Heal);
                        break;
                    case "DistanceWeaponSC":
                    case "MeleeWeaponSC":
                        WeaponSC currentItemW = (WeaponSC)ItemData;
                        LevelManager.Instance.WeaponsController.addWeapon(currentItemW);
                        break;
                }
                Destroy(gameObject);
            }
        }
    }
}
