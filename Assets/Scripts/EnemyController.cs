﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    Waiting,
    FollowPlayer
}
public class EnemyController : EnemyDeadController
{
    public int ExplodeDamage;
    public EnemyState State = EnemyState.Waiting;
    public Animator Animator;
    public SpriteRenderer SpriteRenderer;
    public int ViewRange;
    private Vector2 direccio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        seeForPlayer();
        iAmStunt();
        switch (State)
        {
            case EnemyState.Waiting:
                direccio = Vector2.zero;
                break;
            case EnemyState.FollowPlayer:
                followPlayer();
                break;
        }
    }
    private void iAmStunt()
    {
        if (stuntTime > 0)
        {
            Animator.SetBool("ViewPlayer", false);
            State = EnemyState.Waiting;
            stuntTime -= Time.deltaTime;
        }
    }
    private void followPlayer()
    {
        if ((LevelManager.Instance.Player.transform.position - transform.position).x > 0)
        {
            SpriteRenderer.flipX = false;
        }
        else
        {
            SpriteRenderer.flipX = true;
        }
        direccio = (LevelManager.Instance.Player.transform.position - transform.position);
    }
    private void seeForPlayer()
    {
        if (LevelManager.Instance.Player!=null)
        {
            Vector2 posPlayer = LevelManager.Instance.Player.transform.position;
            Vector2 thisPosition = transform.position;
            if (Mathf.Abs((posPlayer-thisPosition).x)<ViewRange&&(Mathf.Abs((posPlayer - thisPosition).y) < ViewRange))
            {
                Animator.SetBool("ViewPlayer",true);
                State = EnemyState.FollowPlayer;
            }
        }else
            {
                Animator.SetBool("ViewPlayer", false);
                State = EnemyState.Waiting;
            }  
        
    }
    
    private void FixedUpdate()
    {
        transform.Translate(direccio.normalized.x * Time.fixedDeltaTime, direccio.normalized.y * Time.fixedDeltaTime, 0);
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            LevelManager.Instance.playerLostLive(ExplodeDamage);
            LevelManager.Instance.Enemies--;
            Destroy(gameObject);
        }
    }
}
