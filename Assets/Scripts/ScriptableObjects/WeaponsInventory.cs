﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponsInventory", menuName = "ScriptableObjects/Weapons/WeaponsInventory", order = 1)]
public class WeaponsInventory : ScriptableObject
{
    public List<WeaponSC> AvariableWeapons;
    public List<WeaponSC> InitialWeapons;
    
}
