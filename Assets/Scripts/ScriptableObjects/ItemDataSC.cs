﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes
{
    HealKit,
    DistanceWeapon,
    MeleeWeapon
}
public abstract class ItemDataSC : ScriptableObject
{
    public Sprite IconSprite;
    internal ItemTypes ItemType;
    public string Name;
    public int Price;
}
