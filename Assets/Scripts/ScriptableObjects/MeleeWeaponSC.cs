﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MeleeWeaponData", menuName = "ScriptableObjects/Weapons/MeleeWeaponData", order = 1)]
public class MeleeWeaponSC : WeaponSC
{
    void Awake()
    {
        ItemType = ItemTypes.MeleeWeapon;
    }
}
