﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DistanceWeaponData", menuName = "ScriptableObjects/Weapons/DistanceWeaponData", order = 1)]
public class DistanceWeaponSC : WeaponSC
{
    void Awake(){
        ItemType = ItemTypes.DistanceWeapon;
    }
    
    public int BulletXShoot;

    public GameObject Bullet;

    public float ReloadTime;

    public int BulletAmount;
    public int MaxBulletCapacity;

    public Vector2 OffsetFirePoint;
}
