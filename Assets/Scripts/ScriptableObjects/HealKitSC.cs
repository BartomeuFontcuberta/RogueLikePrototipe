﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealKit", menuName = "ScriptableObjects/HealKit", order = 1)]
public class HealKitSC : ItemDataSC
{
    void Awake()
    {
        ItemType = ItemTypes.HealKit;
    }
    public int Heal;

}
