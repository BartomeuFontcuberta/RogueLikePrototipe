﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemListDrop", menuName = "ScriptableObjects/ItemListDrop", order = 1)]
public class ItemListDrop : ScriptableObject
{
    public List<ItemDataSC> items;

    public GameObject ItemDropablePrefab;

    public GameObject getRandomItem()
    {
        //Random funtionality
        GameObject item = Instantiate(ItemDropablePrefab);
        ItemDataSC itemData = items[Random.Range(0, items.Count)];

        item.GetComponent<ItemController>().ItemData = itemData;
        return item;
    }
}

