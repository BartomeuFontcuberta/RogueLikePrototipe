﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSC : ItemDataSC
{
    public Sprite WeaponSprite;
    public int Damage;
    public float FireRate;
    //Força de retrocés
    public float RecoilForce;
}
