﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyDeadController : MonoBehaviour
{
    public int Live;
    public int DeadPoints;
    internal float stuntTime=0;
    public AudioClip DeadSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void makeDamage(int damage)
    {
        Live -= damage;
        LevelManager.Instance.addPoints(damage);
        if (Live <= 0)
        {
            LevelManager.Instance.EnemiesKilled++;
            dead();
        }
    }
    /*public IEnumerator Stunt(float stuntTime)
    {
        yield return new WaitForSeconds(stuntTime);
    }*/
    internal void dead()
    {
        LevelManager.Instance.addPoints(DeadPoints);
        LevelManager.Instance.Enemies--;
        LevelManager.Instance.gameObject.GetComponent<AudioSource>().clip=DeadSound;
        LevelManager.Instance.gameObject.GetComponent<AudioSource>().Play();
        Destroy(gameObject);
    }
}
