﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    private static ShopManager _instance;
    public static ShopManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject[] ItemSpawnPoints;
    public GameObject Item;
    public WeaponsInventory Weapons;
    public ShopUIController UIController;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < ItemSpawnPoints.Length; i++)
        {
            if (Weapons.AvariableWeapons.Count > i)
            {
                GameObject item = Instantiate(Item, ItemSpawnPoints[i].transform.position,Quaternion.identity);
                item.GetComponent<ItemController>().ItemData = Weapons.AvariableWeapons[i];
            }
            Destroy(ItemSpawnPoints[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
