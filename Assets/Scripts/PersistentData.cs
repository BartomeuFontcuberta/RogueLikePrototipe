﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class PersistentData
{
    private static string folder = "/saveData/";
    private static string fExtension = ".myGame";

    public static void SaveData<T>(T objectToSave, string name)
    {
        string path = Application.persistentDataPath + folder;

        Directory.CreateDirectory(path);

        BinaryFormatter bformatter = new BinaryFormatter();

        using(FileStream fStream = new FileStream(path+name+fExtension, FileMode.Create))
        {
            bformatter.Serialize(fStream, objectToSave);
        }
    }

    public static T LoadData<T>(T objectToSave, string name)
    {
        string path = Application.persistentDataPath + folder+ name + fExtension;
        Debug.Log(File.Exists(path));
        if (File.Exists(path))
        {
            BinaryFormatter bformatter = new BinaryFormatter();
            using (FileStream fStream = new FileStream(path , FileMode.Open))
            {
                return (T)bformatter.Deserialize(fStream);
            }
        }
        return default(T);
    }
}
