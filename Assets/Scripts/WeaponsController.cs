﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsController : MonoBehaviour
{
    public WeaponsInventory Inventory;
    public List<WeaponSC> Weapons;
    public List<WeaponSC> InitialWeapons;
    public int ActiveWeaponIndex = 2;
    // Start is called before the first frame update
    void Start()
    {
        InitialWeapons = Inventory.InitialWeapons;
        foreach (WeaponSC newWeapon in InitialWeapons)
        {
            Weapons.Add(Instantiate(newWeapon));  
        }
        LevelManager.Instance.UIController.updateWeapons(Weapons,ActiveWeaponIndex);
        LevelManager.Instance.Gun.GetComponent<GunController>().changeWeapon(Weapons[ActiveWeaponIndex]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            changeWeapon(true);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0) {
            changeWeapon(false);
        }
    }
    private void changeWeapon(bool direction)
    {
        if (direction)
        {
            ActiveWeaponIndex++;
            if (ActiveWeaponIndex > Weapons.Count-1)
            {
                ActiveWeaponIndex = 0;
            }
        }
        else
        {
            ActiveWeaponIndex--;
            if (ActiveWeaponIndex < 0){
                ActiveWeaponIndex = Weapons.Count-1;
            }
        }
        LevelManager.Instance.UIController.updateWeapons(Weapons,ActiveWeaponIndex);
        if (LevelManager.Instance.Gun != null)
        {
            LevelManager.Instance.Gun.GetComponent<GunController>().changeWeapon(Weapons[ActiveWeaponIndex]);
        }
    }
    public void addWeapon(WeaponSC newWeapon)
    {
        bool added = false;
        for (int i = 0; i < Weapons.Count; i++)
        {
            if (Weapons[i].Name == newWeapon.Name)
            {
                if(newWeapon.GetType().Name== "DistanceWeaponSC")
                {
                    DistanceWeaponSC newWeaponDW = (DistanceWeaponSC)newWeapon;
                    DistanceWeaponSC currentItemDW = (DistanceWeaponSC)Weapons[i];
                    currentItemDW.BulletAmount += newWeaponDW.BulletAmount;
                    if (currentItemDW.BulletAmount > currentItemDW.MaxBulletCapacity)
                    {
                        currentItemDW.BulletAmount = currentItemDW.MaxBulletCapacity;
                    }
                }
                added = true;
                break;
            }
        }
        if (!added)
        {
            Weapons.Add(Instantiate(newWeapon));
        }
        changeWeapon(false);
        changeWeapon(true);
    }
    public void useBullet()
    {
        if (Weapons[ActiveWeaponIndex].GetType().Name == "DistanceWeaponSC")
        {
            DistanceWeaponSC currentItemDW = (DistanceWeaponSC)Weapons[ActiveWeaponIndex];
            currentItemDW.BulletAmount--;
            LevelManager.Instance.UIController.reloadBullets(currentItemDW.BulletAmount);
        }
    }
    public bool canShot()
    {
        if (Weapons[ActiveWeaponIndex].GetType().Name == "DistanceWeaponSC")
        {
            DistanceWeaponSC currentItemDW = (DistanceWeaponSC)Weapons[ActiveWeaponIndex];
            if (currentItemDW.BulletAmount > 0){
                return true;
            }
        }
        return false;
    }
}
