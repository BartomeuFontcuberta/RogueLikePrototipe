﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataToSave
{
    public int Points;
    public int MaxPoints;
    public int LastPoints;
    //public WeaponsInventory WeaponInventory;
}
